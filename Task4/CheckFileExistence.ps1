﻿<#
Check local file system if file will be there then print out something and "elif" file will not exists just, download this from some URL.
#>
param(
    [Parameter(Mandatory)][string]$FIleSystemPAth,
    [Parameter(Mandatory)][string]$File,
    [Parameter(Mandatory)][string]$URL
)
if(Test-Path $FIleSystemPAth\$File){
    Write-Host "The specified file exists in the local file system!"}
else{
    Write-Host "The specified file does not exist, it will be downloaded from $URL."
    Invoke-WebRequest -Uri $URL -OutFile $FIleSystemPAth\$File }