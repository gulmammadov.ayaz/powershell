﻿<#
Create powershell script which makes 10 copies of this XML and change value of the line (at your choice) with new value defined in parameters.
#>
param(
    [Parameter(Mandatory)][string]$xmlFile,
    [Parameter(Mandatory)][string]$SavePath
)

for ($i = 1; $i -le 10; $i ++) {
    $xml = New-Object xml
    $xml.Load($xmlFile)
    $element = ($xml.SelectNodes("//th"))
    $element[0].InnerText = "Title$i"
    $element[1].InnerText = "Artist$i"
    $xml.Save("$SavePath\XMLDoc$i.xml")
}

