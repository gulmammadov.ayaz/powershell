﻿<#
Create one script which will call another.
Second script must execute some calculations using parameters passed from main script.
#>
param(
[Parameter(Mandatory)][double]$Number1,
[Parameter(Mandatory)][double]$Number2
)
.\SecondScript.ps1 $Number1 $Number2