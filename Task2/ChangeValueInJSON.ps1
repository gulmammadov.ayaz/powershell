﻿<#
Take this JSON file and update value of the 'SortAs' to 'OMPL' and then save file.
#>
param(
[Parameter(Mandatory)][string]$JSONFilePath
)
$json = get-content -Raw $JSONFilePath | ConvertFrom-Json
$attributes = $json.glossary.GlossDiv.GlossList.GlossEntry
foreach($attribute in $attributes){$attribute.SortAs = "OMPL"}
$json | ConvertTo-Json -Depth 100 | Set-Content $JSONFilePath