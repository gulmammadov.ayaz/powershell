﻿<#
Create script using switch parameter. If parameter is true execute get-disk cmdlet.
#>
param(
    [switch]$MyParam)
    if($MyParam){Write-Host "Here is your disks list:" 
    Get-Disk}
    else{Write-Host "If you want to get local disks use MyParam parameter "}
#So if I save script and call it like ".\SwitchParam.ps1 -MyParam" it will execute Get-Disk cmdlet           