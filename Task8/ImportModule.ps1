﻿<#
Create script which will call PSM.
PSM must execute some calculations using parameters passed from main script.
#>
param(
    [double]$Number1,
    [double]$Number2
)
Import-Module .\PSModule.psm1
Get-Sum $Number1 $Number2