﻿#Create object and fill with values
$pcname = (Get-WmiObject win32_computersystem).name
$pcmodel = (Get-WmiObject win32_computersystem).model
$OS = (Get-WmiObject win32_operatingSystem).caption
$SN = (Get-WmiObject win32_bios).serialnumber
#Lets create a powershell object which contains comp name, operating system, pc model and serial number
$SystemInfo = New-Object psobject
$SystemInfo | Add-Member -MemberType NoteProperty -Name ComputerName -Value $pcname
$SystemInfo | Add-Member -MemberType NoteProperty -Name PCModel -Value $pcmodel
$SystemInfo | Add-Member -MemberType NoteProperty -Name OperatingSystem -Value $OS
$SystemInfo | Add-Member -MemberType NoteProperty -Name SerialNumber -Value $SN