﻿<#
Check that variable is undefined then, set new value and print out, "elif" print out value (Check for both Null and Empty variable).
#>
param(
    [Parameter(Mandatory)]$variableName,
    [Parameter(Mandatory)]$value
)
if(!(Test-Path variable:$variableName)){
    Write-Host "The specified variable does not exist. It will be defined to $value"
    New-Variable -Name $variableName -Value $value -Force}
elseif((Get-Variable -Name $variableName).Value -eq ""){
    Write-Host "The specified variable is empty. It will be defined to $value"
    New-Variable -Name $variableName -Value $value -Force}
else{Write-host "The specified variable exists. It's value is" (Get-Variable -Name $variableName).Value}
