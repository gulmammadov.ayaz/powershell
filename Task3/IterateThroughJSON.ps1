﻿<#
Use loop to iterate through JSON file and print out all keys and values equal to defined parameter.
#>
param(
    [Parameter(Mandatory)][string]$JsonFilePath
)
$json = Get-Content -Raw $JsonFilePath | ConvertFrom-Json
$json | ForEach-Object{Write-Host "Name:"$_.name, "AppName:"$_.appname, "RgName:"$_.rgname}